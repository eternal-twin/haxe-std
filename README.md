# Haxe Std

TypeScript port of parts of the Haxe standard library.

It corresponds to the version 3.4 for the `js` target.
