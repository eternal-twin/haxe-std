import { HX_NAME } from "./_symbols.js";

export const Float = Object.assign(Number, {[HX_NAME]: ["Float"]});

export class Int {
  public static readonly [HX_NAME]: readonly string[] = ["Int"];
}

export type Null<T> = T | null | undefined;

export const Bool = Object.assign(Boolean, {[HX_NAME]: ["Bool"]});

export class Dynamic {
  public static readonly [HX_NAME]: readonly string[] = ["Dynamic"];
}
