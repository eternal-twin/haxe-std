import { HX_NAME } from "./_symbols.js";

const _String = Object.assign(String, {[HX_NAME]: ["String"]});
export { _String as String };
