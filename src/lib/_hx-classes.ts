import { Enum } from "./enum";
import { EnumValue } from "./enum-value";
import { Class, ClassValue } from "./class";

export const HX_CLASSES: Map<String, Enum<EnumValue> | Class<ClassValue>> = new Map();
