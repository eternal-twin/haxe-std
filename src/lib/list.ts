import { HX_NAME } from "./_symbols.js";

export class List<T> {
  public static readonly [HX_NAME]: readonly string[] = ["List"];

  private h: ListNode<T> | null;
  private q: ListNode<T> | null;
  private _length: number;

  /**
   * The length of `this` List.
   */
  public get length(): number {
    return this._length;
  }

  /**
   * Creates a new empty list.
   */
  public constructor() {
    this.h = null;
    this.q = null;
    this._length = 0;
  }

  /**
   * Adds element `item` at the end of `this` List.
   *
   * `this.length` increases by 1.
   */
  public add(item: T): void {
    const x: ListNode<T> = ListNode.create(item, null);
    if (this.h === null) {
      this.h = x;
    } else {
      this.q!.next = x;
    }
    this.q = x;
    this._length++;
  }
}

class ListNode<T> {
  public static readonly [HX_NAME]: readonly string[] = ["List", "ListNode"];

  public item: T;
  public next: ListNode<T> | null;

  public constructor(item: T, next: ListNode<T> | null) {
    this.item = item;
    this.next = next;
  }

  public static create<T>(item: T, next: ListNode<T> | null): ListNode<T> {
    return new ListNode(item, next);
  }
}
