import { HX_CLASS, HX_ENAME, HX_ENUM, HX_NAME } from "../_symbols.js";
import { Bool, Dynamic, Float, Int } from "../std-types.js";
import { String } from "../string.js";
import { Array } from "../array.js";
import { Class } from "../class.js";
import { Enum } from "../enum.js";

export class Boot {
  public static readonly [HX_NAME]: readonly string[] = ["js", "Boot"];

  public static getClass(o: unknown) {
    if (o instanceof Array) {
      return Array;
    } else {
      return Reflect.get(o as object, HX_CLASS);
    }
  };

  public static __string_rec(o: unknown, s: string): string {
    if (o === null || o === undefined) {
      return "null";
    }
    if (s.length >= 5) {
      return "<...>"; // Too much deep recursion
    }
    switch (typeof o) {
      case "object": {
        if (o instanceof Array) {
          let str: string = "[";
          s += "\t";
          for (let i = 0; i < (o as any).length; i++) {
            str += (i > 0 ? "," : "") + Boot.__string_rec((o as any)[i], s);
          }
          str += "]";
          return str;
        } else {
          throw new Error("NotImplemented");
        }
      }
      case "function": {
        return "<function>";
      }
      case "string":
        return o;
      default:
        return String(o);
    }
  }

  public static __instanceof(o: unknown, cl: unknown): boolean {
    switch (cl) {
      case null:
      case undefined:
        return false;
      case Int:
        return (o as number | 0) === o;
      case Float:
        return typeof o === "number";
      case Bool:
        return typeof o === "boolean";
      case String:
        return typeof o === "string";
      case Array:
        return o instanceof Array;
      case Dynamic:
        return true;
      default: {
        if (o === null || o === undefined) {
          return false;
        }
        if (typeof cl === "function") {
          if (o instanceof cl) {
            return true;
          }
          if (Boot.__interfLoop(Boot.getClass(o), cl)) {
            return true;
          }
        }
        if (cl === Class && Reflect.has(o as object, HX_CLASS)) {
          return true;
        }
        if (cl === Enum && Reflect.has(o as object, HX_ENAME)) {
          return true;
        }
        return Reflect.get(o as object, HX_ENUM) === cl;
      }
    }
  }

  public static __interfLoop(cc: unknown, cl: unknown): boolean {
    if (cc === null) {
      return false;
    }
    if (cc === cl) {
      return true;
    }
    throw new Error("NotImplemented");
  }
}
