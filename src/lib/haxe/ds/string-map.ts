import { HX_NAME } from "../../_symbols.js";

export class StringMap<T> implements Iterable<T> {
  public static readonly [HX_NAME]: readonly string[] = ["haxe", "ds", "StringMap"];

  private readonly inner: Map<string, T>;

  public constructor() {
    this.inner = new Map();
  }

  set(key: string, value: T): void {
    this.inner.set(key, value);
  }

  get(key: string): T | null {
    return this.inner.has(key) ? this.inner.get(key)! : null;
  }

  exists(key: string): boolean {
    return this.inner.has(key);
  }

  remove(key: string): boolean {
    return this.inner.delete(key);
  }

  keys(): IterableIterator<string> {
    return this.inner.keys();
  }

  [Symbol.iterator](): IterableIterator<T> {
    return this.inner.values();
  }

  toString(): string {
    throw new Error("NotImplemented: StringMap#toString");
  }
}
