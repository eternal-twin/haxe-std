import { StringMap } from "./ds/string-map.js";
import { StringTools } from "../string-tools.js";
import { Reflect } from "../reflect.js";
import { Math } from "../math.js";
import { Type } from "../type.js";
import { Std } from "../std.js";
import { HX_NAME } from "../_symbols.js";
import { List } from "../list.js";
import { Enum } from "../enum.js";
import { EnumValue } from "../enum-value.js";
import { Class, ClassValue } from "../class.js";

export interface TypeResolver {
  resolveClass(name: String): Class<ClassValue> | null;

  resolveEnum(name: String): Enum<EnumValue> | null;
}

class DefaultResolver implements TypeResolver {
  public static readonly [HX_NAME]: readonly string[] = ["haxe", "Unserializer", "DefaultResolver"];

  public resolveClass(name: string): Class<ClassValue> | null {
    return Type.resolveClass(name);
  }

  public resolveEnum(name: string): Enum<EnumValue> | null {
    return Type.resolveEnum(name);
  }
}

class NullResolver implements TypeResolver {
  public static readonly [HX_NAME]: readonly string[] = ["haxe", "Unserializer", "NullResolver"];

  private static _instance: NullResolver | undefined;

  public resolveClass(): null {
    return null;
  }

  public resolveEnum(): null {
    return null;
  }

  public static get instance(): NullResolver {
    if (NullResolver._instance === undefined) {
      NullResolver._instance = new NullResolver();
    }
    return NullResolver._instance;
  }
}

export class Unserializer {
  public static readonly [HX_NAME]: readonly string[] = ["haxe", "Unserializer"];

  /**
   * We mark this variable as a readonly to avoid actions at distance.
   * Use the instance member `resolver``if you need to configure it.
   */
  public static readonly DEFAULT_RESOLVER: TypeResolver = new DefaultResolver();

  private static readonly BASE64: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
  private static CODES: null | readonly number[] = null;

  private static initCodes(): number[] {
    const codes: number[] = [];
    for (let i: number = 0; i < Unserializer.BASE64.length; i++) {
      codes[StringTools.fastCodeAt(Unserializer.BASE64, i)] = i;
    }
    return codes;
  }

  private readonly buf: string;
  private pos: number;
  private readonly length: number;
  private readonly cache: unknown[];
  private readonly scache: string[];
  private resolver: TypeResolver;

  public constructor(buf: string) {
    this.buf = buf;
    this.length = buf.length;
    this.pos = 0;
    this.scache = [];
    this.cache = [];
    this.resolver = Unserializer.DEFAULT_RESOLVER;
  }

  public setResolver(r: TypeResolver | null | undefined): void {
    if (r === null || r === undefined) {
      this.resolver = NullResolver.instance;
    } else {
      this.resolver = r;
    }
  }

  public getResolver(): TypeResolver {
    return this.resolver;
  }

  private get(p: number): number {
    return StringTools.fastCodeAt(this.buf, p);
  }

  private readDigits(): number {
    let k: number = 0;
    let s: boolean = false;
    let fpos: number = this.pos;
    while (true) {
      const c: number = this.get(this.pos);
      if (StringTools.isEof(c)) {
        break;
      }
      if (c === "-".codePointAt(0)!) {
        if (this.pos !== fpos) {
          break;
        }
        s = true;
        this.pos++;
        continue;
      }
      if (c < "0".codePointAt(0)! || c > "9".codePointAt(0)!) {
        break;
      }
      k = k * 10 + (c - "0".codePointAt(0)!);
      this.pos++;
    }
    if (s) {
      k *= -1;
    }
    return k;
  }

  private readFloat(): number {
    let p1: number = this.pos;
    while (true) {
      const c = this.get(this.pos);
      if (StringTools.isEof(c)) {
        break;
      }
      // + - . , 0-9
      if ((c >= 43 && c < 58) || c == "e".codePointAt(0)! || c == "E".codePointAt(0)!) {
        this.pos++;
      } else {
        break;
      }
    }
    return Std.parseFloat(this.buf.substr(p1, this.pos - p1));
  }

  private unserializeObject(o: object): void {
    while (true) {
      if (this.pos >= this.length) {
        throw new Error("Invalid object");
      }
      if (this.get(this.pos) === "g".codePointAt(0)!) {
        break;
      }
      const k = this.unserialize();
      if (!Std.is(k, String)) {
        throw new Error("Invalid object key");
      }
      const v: unknown = this.unserialize();
      Reflect.setField(o, k as string, v);
    }
    this.pos++;
  }

  private unserializeEnum(edecl: unknown, tag: string): unknown {
    if (this.get(this.pos++) !== ":".codePointAt(0)!) {
      throw new Error("Invalid enum format");
    }
    let nargs: number = this.readDigits();
    if (nargs === 0) {
      return Type.createEnum(edecl as Enum<EnumValue>, tag);
    }
    const args: unknown[] = [];
    while (nargs-- > 0) {
      args.push(this.unserialize());
    }
    return Type.createEnum(edecl as Enum<EnumValue>, tag, args);
  }

  public unserialize(): unknown {
    switch (this.get(this.pos++)) {
      case "n".codePointAt(0)!: {
        return null;
      }
      case "t".codePointAt(0)!: {
        return true;
      }
      case "f".codePointAt(0)!: {
        return false;
      }
      case "z".codePointAt(0)!: {
        return 0;
      }
      case "i".codePointAt(0)!: {
        return this.readDigits();
      }
      case "d".codePointAt(0)!: {
        return this.readFloat();
      }
      case "y".codePointAt(0)!: {
        const len: number = this.readDigits();
        if (this.get(this.pos++) !== ":".codePointAt(0)! || this.length - this.pos < len) {
          throw new Error("Invalid string length");
        }
        let s: string = this.buf.substr(this.pos, len);
        this.pos += len;
        s = StringTools.urlDecode(s);
        this.scache.push(s);
        return s;
      }
      case "k".codePointAt(0)!: {
        return Math.NaN;
      }
      case "m".codePointAt(0)!: {
        return Math.NEGATIVE_INFINITY;
      }
      case "p".codePointAt(0)!: {
        return Math.POSITIVE_INFINITY;
      }
      case "a".codePointAt(0)!: {
        const a: unknown[] = [];
        this.cache.push(a);
        while (true) {
          const c: number = this.get(this.pos);
          if (c === "h".codePointAt(0)!) {
            this.pos++;
            break;
          }
          if (c === "u".codePointAt(0)!) {
            this.pos++;
            const n: number = this.readDigits();
            a[a.length + n - 1] = null;
          } else {
            a.push(this.unserialize());
          }
        }
        return a;
      }
      case "o".codePointAt(0)!: {
        const o: object = {};
        this.cache.push(o);
        this.unserializeObject(o);
        return o;
      }
      case "r".codePointAt(0)!: {
        const n: number = this.readDigits();
        if (n < 0 || n >= this.cache.length) {
          throw new Error("Invalid reference");
        }
        return this.cache[n];
      }
      case "R".codePointAt(0)!: {
        const n: number = this.readDigits();
        if (n < 0 || n >= this.scache.length) {
          throw new Error("Invalid string reference");
        }
        return this.scache[n];
      }
      case "x".codePointAt(0)!: {
        throw this.unserialize();
      }
      case "c".codePointAt(0)!: {
        const name: unknown = this.unserialize();
        const cl: unknown = this.resolver.resolveClass(name as string);
        if (cl === null || cl === undefined) {
          throw new Error(`Class not found ${name}`);
        }
        const o = Type.createEmptyInstance(cl as Class<ClassValue>);
        this.cache.push(o);
        this.unserializeObject(o);
        return o;
      }
      case "w".codePointAt(0)!: {
        const name: unknown = this.unserialize();
        const edecl: unknown = this.resolver.resolveEnum(name as string);
        if (edecl === null || edecl === undefined) {
          throw new Error(`Enum not found ${name}`);
        }
        const e: unknown = this.unserializeEnum(edecl, this.unserialize() as string);
        this.cache.push(e);
        return e;
      }
      case "j".codePointAt(0)!: {
        const name: unknown = this.unserialize();
        const edecl: Enum<EnumValue> | null = this.resolver.resolveEnum(name as string);
        if (edecl === null || edecl === undefined) {
          throw new Error(`Enum not found ${name}`);
        }
        this.pos++; // Skip ':'
        const index: number = this.readDigits();
        const tag = Type.getEnumConstructs(edecl)[index];
        if (tag === null) {
          throw new Error(`Unknown enum index ${name}@${index}`);
        }
        const e: unknown = this.unserializeEnum(edecl, tag);
        this.cache.push(e);
        return e;
      }
      case "l".codePointAt(0)!: {
        const l: List<unknown> = new List();
        this.cache.push(l);
        while (this.get(this.pos) !== "h".codePointAt(0)) {
          return l.add(this.unserialize());
        }
        this.pos++;
        return l;
      }
      case "b".codePointAt(0)!: {
        const h: StringMap<unknown> = new StringMap();
        this.cache.push(h);
        while (this.get(this.pos) !== "h".codePointAt(0)) {
          const s: unknown = this.unserialize();
          return h.set(s as string, this.unserialize());
        }
        this.pos++;
        return h;
      }
      case "q".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "M".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "v".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "s".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "C".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "A".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      case "B".codePointAt(0)!: {
        throw new Error("NotImplemented");
      }
      default:
        this.pos--;
        throw new Error(`Invalid char ${this.buf.charAt(this.pos)} at position ${this.pos}`);
    }
  }

  public static run(v: string): unknown {
    return new Unserializer(v).unserialize();
  }
}
